(() => {
  const description = document.querySelector("[itemprop=description]") ?? document.querySelector(".vacancy-section");
  const companyName = document.querySelector("span.vacancy-company-name");

  const getCoverLetter = async (description, cv) => {
    const response = await fetch(
      'https://6c08-92-242-62-145.ngrok-free.app/coverletter',
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          description: description,
          cv: cv,
        }),
      });
    if (response.ok) {
      return JSON.parse(await response.text());
    } else {
      return "Ошибка HTTP: " + response.status;
    }
  }

  const getCVCode = async (url) => {
    const response = await fetch(
      url,
      {
        method: "GET",
      }
    );

    if (response.ok) {
      return await response.text();
    } else {
      return "Ошибка HTTP: " + response.status;
    }
  }

  const getCVText = async (url, callbackfn) => {
    getCVCode(url).then((result) => {
      result = result.replace('<!DOCTYPE html>', '');
      let el = document.createElement('div');

      // Так как сайт написан на реакте парсить страницу нельзя.
      // Приходится брать её код и монтировать в невидимый div размера 1x1
      // С api данные брать тяжело, т.к. api платное или местами закрытое.
      el.innerHTML = '<div style="width: 1px; height: 1px; overflow: hidden">' + result + "</div>";
      description.appendChild(el);

      setTimeout(function () {
        const getText = (tag) => {
          const elem = document.querySelector(tag);
          return elem.textContent;
        }
        let body = document.querySelector('.resume-wrapper');

        let bodyText = '';
        body.querySelectorAll('div>span, li, p').forEach((elem, key, parent) => {
          if (elem.textContent !== 'редактировать' && elem.textContent !== 'контактный телефон') {
            bodyText = bodyText + elem.textContent + '\n';
          }
        });

        const cv = getText('.resume-header-name') + '\n' + getText('.resume-header-title p') + '\n' + bodyText;
        callbackfn(cv);
      }, 1000);
    });
  }

  if (description) {
    const text = 'Компания: ' + companyName.textContent + '\n' + description.textContent;
    console.log('text:' + text);

    description.insertAdjacentHTML('beforebegin', '<style>.loader { margin-top: 12px; border: 16px solid #f3f3f3; /* Light grey */ border-top: 16px solid #3498db; /* Blue */ border-radius: 50%; width: 48px; height: 48px; animation: spin 10s linear infinite; } @keyframes spin { 0% { transform: rotate(0deg); } 100% { transform: rotate(360deg); }}</style><div id="hhassist" style="border: 2px solid #757575; border-radius: 10px; padding: 16px; margin-bottom: 8px"><p>Перейдите на <a href="https://hh.ru/applicant/resumes" target="_blank" rel="noopener noreferrer">страницу со списком резюме</a>, откройте нужное резюме и скопируйте ссылку на него</p><br/><label for="url">Url на резюме вида: https://*.hh.ru/resume/aAbBcC</label><br/><input type="text" id="url" name="url" required minlength="10" size="20"><button id="onUrl" style="margin-left: 16px">Сгенерировать</button></div>');

    document.getElementById('onUrl').addEventListener('click', () => {
      const url = document.getElementById('url').value;
      console.log("url: " + url);
      document.getElementById('hhassist').innerHTML = '<p>Получение резюме...</p>';

      getCVText(url, (cv) => {
        document.getElementById('hhassist').innerHTML = '<p>Генерация... (процесс может занимать до 30 секунд)</p><div class="loader"></div>';
        console.log('cv:' + cv);

        getCoverLetter(text, cv).then((result) => {
          result = result.replaceAll('\n', '<br/>');
          document.getElementById('hhassist').innerHTML = '<p><b>Вот текст вашего сопроводительного письма:</b></p><p>' + result + '</p>';
          console.log('result:' + result);
        });
      });
    });
  }
})();
